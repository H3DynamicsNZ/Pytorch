# pytorch (WIP)

Pytorch Model and Dashboard

![alt text](images/cli.png)

![alt text](images/web.png)


## Usage

```
usage: PROG [-h] [-v] [-t] [-s] [-i] [-c [CLEAR [CLEAR ...]]]

Process some integers.

optional arguments:
  -h, --help            show this help message and exit
  -v, --version         show program's version number and exit
  -t, --train           Runs Training, CTRL+C to Exit.
  -s, --stats           Outputs Plots and JSON
  -i, --info            Outputs Model Information to CLI
  -c [CLEAR [CLEAR ...]], --clear [CLEAR [CLEAR ...]]
                        Clears State Data Usage: all,hist,opt
```
